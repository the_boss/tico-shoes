from django.urls import path, re_path
from . import views



app_name = 'catalogue'


urlpatterns = [
    path('',  views.AboutView.as_view(), name='about'),
    path('stores/',  views.IndexView.as_view(), name='stores'),
    path('test/',  views.CollectionView.as_view(), name='collection'),
    path('collection/',  views.TestView.as_view(), name='test'),
    path('catalogue/',  views.CatalogueView.as_view(), name='catalogue'),

    path('deals/', views.DealsView.as_view(), name='deals'),
    path('franquicia/', views.FranquiciaView.as_view(), name='franquicia'),
    path('contact/', views.ContactView.as_view(), name='contact'),
    path('detail/',  views.getProduct, name='detail'),
    path('contactme/',  views.contact, name='contact_me'),
    path('request/',  views.request, name='request_me'),


]
