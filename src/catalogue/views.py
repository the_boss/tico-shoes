from django.views.generic import ListView, TemplateView
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, TemplateView
from django.shortcuts import render

from .models import Product, Year, Deals, Contact, Request, ProductFilter
from django.http import HttpResponse
import json
from django.core import serializers

@csrf_exempt
def contact(request):
    if request.method == "POST" and request.is_ajax():
        name = request.POST['name']
        email = request.POST['email']
        content = request.POST['content']

        contact = Contact(name=name, email=email, content= content)
        contact.save()
        status = 200
        return HttpResponse(status)

    else:
        print("ERRROR")
        status = 500
        return HttpResponse(status)


@csrf_exempt
def request(request):
    print("data = ",request)
    if request.method == "POST" and request.is_ajax():
        name = request.POST['name']
        email = request.POST['email']
        content = request.POST['content']

        request = Request(name=name, email=email, content= content)
        request.save()
        status = 200
        return HttpResponse(status)

    else:
        print("ERRROR")
        status = 500
        return HttpResponse(status)



class ContactView(TemplateView):
    template_name = "contact.html"
    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['active'] = "Contact"
        return context




class IndexView(TemplateView):
    # model = Product
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['active'] = "Stores"
        return context



class AboutView(TemplateView):
    # model = Product
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        context['active'] = "About"
        return context



class CollectionView(TemplateView):
    model = Year
    template_name = "collection.html"
    context_object_name = 'object'

    def get_context_data(self, **kwargs):
        context = super(CollectionView, self).get_context_data(**kwargs)
        context['active'] = "Collection"
        root = Year.objects._mptt_filter(parent=None)
        object_list = Year.objects.all().get_descendants(include_self=True).filter(level=1)
        context['years'] = root
        context['list'] = object_list
        # print("Roots", root)
        # context.items()
        print("content", context)
        return context


class CatalogueView(ListView):
    model = Product

    template_name = "catalogue.html"
    context_object_name = 'products'
    paginate_by = 4

    def get_context_data(self, **kwargs):
        context = super(CatalogueView, self).get_context_data(**kwargs)
        context.items()
        context['active'] = "Collection"

        return context

class TestView(ListView):
    model = Product

    template_name = "test.html"
    context_object_name = 'products'
    paginate_by = 4




    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)
        context.items()
        data = self.request.GET

        cat = data.getlist('categories[]')
        gen = data.getlist('gender[]')
        print(gen)

        context['nodes'] = Year.objects.all()
        context['active'] = "Collection"

        gender = []

        if '-1' in gen:
            gender.append("Мальчики")
            # gender.append("Унисекс")
        if '-2' in gen:
            gender.append("Девочки")
            # gender.append("Унисекс")
        if '-3' in gen:
            gender.append("Унисекс")

        if gen ==[] and cat == []:
            gender = ["Мальчики", "Девочки", "Унисекс"]
            qs = Product.objects.all()
        else:
            qs = Product.objects.all().filter(sex__in=gender).filter(year__id__in=cat)
        context["object_list"] = qs
        context["gender"] = gender



        # f = ProductFilter(queryset=Product.objects.all()).qs
        # print("F", f)
        # return render(request, 'my_app/template.html', {'filter': f})


        return context




def getProduct(request):
    print(request)
    if request.method == "GET":
        print(request)
        slug = request.GET.get('slug')
        product = Product.objects.get(slug=slug)

        json = product.get_json()
        images = product.get_images_url
        pic = []
        for img in images:
            pic.append(img.image.url)
        json["pic"] = pic

        return JsonResponse(json,  safe=True)
        # result = serializers.serialize('json', [product ])
        # return HttpResponse(result[1:-1], content_type='application/json; charset=UTF-8',
        #                     status=200)
    else:
        return JsonResponse(data={'success': False,
                                  'errors': 'No matching items found'})




class DealsView(ListView):
    model = Deals
    template_name = "deals.html"
    context_object_name = 'deals'

    def get_context_data(self, **kwargs):
        context = super(DealsView, self).get_context_data(**kwargs)
        object_list = Deals.objects.all()
        context['list'] = object_list
        context['active'] = "Deals"
        print(context['list'])
        return context



class FranquiciaView(TemplateView):
    # model = Product
    template_name = "franquicia.html"

    def get_context_data(self, **kwargs):
        context = super(FranquiciaView, self).get_context_data(**kwargs)
        context['active'] = "Franquicia"
        return context
