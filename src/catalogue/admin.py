from django.contrib import admin
from .models import Product, Manufacturer, ProductImage, Year, Material, Contact, Request,Color, Deals
from mptt.admin import MPTTModelAdmin
from mptt.admin import TreeRelatedFieldListFilter
from django import forms


class ManufacturerAdmin(admin.ModelAdmin):
    model = Manufacturer
    prepopulated_fields = {"slug": ('name',)}

class MaterialAdmin(admin.ModelAdmin):
    model = Material


class ColorAdmin(admin.ModelAdmin):
    model = Color


class YearAdmin(MPTTModelAdmin, ):
    mptt_level_indent = 40
    model = Year
    # prepopulated_fields = {"slug": ('name',)}


class ProductImage(admin.TabularInline):
    model = ProductImage




class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'sex',  'material_top', 'material_bottom', 'is_active',)
    list_filter = ('is_active',
                   ('year', TreeRelatedFieldListFilter),
                   )
    ordering = ['-is_active', 'name']
    inlines = [
        ProductImage,
    ]

class ContactAdmin(admin.ModelAdmin):
    model = Contact
    list_display = ('name', 'email', 'content', 'status', 'created_at')


class RequestAdmin(admin.ModelAdmin):
    model = Request
    list_display = ('name', 'email', 'content', 'status', 'created_at')

class DealsAdmin(admin.ModelAdmin):
    model = Deals
    list_display = ('name','content', 'created_at')


admin.site.register(Product, ProductAdmin)
admin.site.register(Year, YearAdmin)
admin.site.register(Deals, DealsAdmin)

admin.site.register(Contact, ContactAdmin)
admin.site.register(Request, RequestAdmin)

admin.site.register(Color, ColorAdmin)
admin.site.register(Material, MaterialAdmin)
admin.site.register(Manufacturer, ManufacturerAdmin)


