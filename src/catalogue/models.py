from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from versatileimagefield.fields import PPOIField, VersatileImageField
import django_filters

CONTACT_CHOICES = (
    ('Непрочитанная', 'Непрочитанная'),
    ('В процессе', 'В процессе'),
    ('Завершенная', 'Завершенная'),

)

SEX_CHOICES = (
    ('Мальчики', 'Мальчики'),
    ('Девочки', 'Девочки'),
    ('Унисекс', 'Унисекс'),

)

TAG_CHOICES = (
    ('New', 'New'),
    ('Хит', 'Хит'),
    ('Sale', 'Sale'),
)


class Contact(models.Model):
    name = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    content = models.TextField(blank=True)
    status = models.CharField(choices=CONTACT_CHOICES, max_length=20, verbose_name=u"Статус", default="Непрочитанная")
    created_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return self.name


class Request(models.Model):
    name = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    content = models.TextField(blank=True)
    status = models.CharField(choices=CONTACT_CHOICES, max_length=20, verbose_name=u"Статус", default="Непрочитанная")
    created_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        verbose_name = 'Франшиза'
        verbose_name_plural = 'Франшизы'

    def __str__(self):
        return self.name


class Material(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name = 'Материал'
        verbose_name_plural = 'Материалы'

    def __str__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name = 'Цвет'
        verbose_name_plural = 'Цвета'

    def __str__(self):
        return self.name


class Manufacturer(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(unique=True)

    class Meta:
        verbose_name = 'Производитель'
        verbose_name_plural = 'Производители'

    def __str__(self):
        return self.name


class Year(MPTTModel):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True, db_index=True)
    slug = models.SlugField(unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE, )

    image = VersatileImageField(
        'Image',
        upload_to='images/',
        width_field='width',
        height_field='height',
        ppoi_field='ppoi',
        blank=True
    )
    height = models.PositiveIntegerField(
        'Image Height',
        blank=True,
        null=True
    )
    width = models.PositiveIntegerField(
        'Image Width',
        blank=True,
        null=True
    )
    ppoi = PPOIField(
        'Image PPOI'
    )

    _metadata = {
        'description': 'description',
    }

    class Meta:
        verbose_name = 'Коллекция'
        verbose_name_plural = 'Коллекции'
        ordering = ['-created_at']

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_image(self):
        print("Image")
        return self.image


class ProductQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)


class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model, using=self.db)

    def all(self):
        return self.get_queryset().active()


class Product(models.Model):
    name = models.CharField(max_length=50, unique=True, verbose_name=u"Название")
    slug = models.SlugField(unique=True)
    manufacturer = models.ForeignKey(Manufacturer, related_name='manufacturers', on_delete=models.CASCADE,
                                     verbose_name=u"Производитель")
    sex = models.CharField(choices=SEX_CHOICES, max_length=20, verbose_name=u"Пол")
    content = models.TextField(blank=True, verbose_name=u"Описание")
    color = models.ForeignKey(Color, related_name='color', on_delete=models.CASCADE,
                              verbose_name=u"Цвет")
    material_top = models.ForeignKey(Material, related_name='top', on_delete=models.CASCADE,
                                     verbose_name=u"Материал верха")
    material_bottom = models.ForeignKey(Material, related_name='bottom', on_delete=models.CASCADE,
                                        verbose_name=u"Материал подкладки")

    size = models.CharField(max_length=50, verbose_name=u"Размерный ряд")
    year = models.ForeignKey(Year, related_name='years', on_delete=models.CASCADE, verbose_name=u"Год")

    tag = models.CharField(blank=True, choices=TAG_CHOICES, max_length=20, verbose_name=u"Тэг")

    is_active = models.BooleanField(default=True, verbose_name=u"Активность")

    created_at = models.DateTimeField(auto_now=True, db_index=True)
    updated_at = models.DateTimeField(auto_now_add=True, db_index=True)

    objects = ProductManager()

    class Meta:
        ordering = ['-name']
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    @property
    def get_images_url(self):
        return self.images.all()


    def get_first_image(self):
        images = list(self.images.all())
        return images[0].image if images else None

    def get_json(self):
        return {'name': self.name, 'manufacturer': self.manufacturer.name,
                'sex': self.sex, 'content': self.content, 'color': self.color.name,
                'material_top': self.material_top.name, 'material_bottom': self.material_bottom.name,
                'size': self.size, 'year': self.name, 'tag': self.tag
                }


class ProductFilter(django_filters.FilterSet):
    sex = django_filters.CharFilter(field_name='sex', lookup_expr='iexact')
    year = django_filters.NumberFilter(field_name='category', lookup_expr='isnull')

    class Meta:
        model = Product
        fields = ['sex', 'year']

    def my_custom_filter(self, queryset, sex__in):
        return queryset.filter(**{
            sex__in: ["Мальчики"],
        })


class ProductImage(models.Model):
    product = models.ForeignKey(
        Product, related_name='images', on_delete=models.CASCADE)

    image = VersatileImageField(
        'Image',
        upload_to='images/',
        width_field='width',
        height_field='height',
        ppoi_field='ppoi'
    )
    height = models.PositiveIntegerField(
        'Высота',
        blank=True,
        null=True
    )
    width = models.PositiveIntegerField(
        'Ширина',
        blank=True,
        null=True
    )
    ppoi = PPOIField(
        'Image PPOI'
    )

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'

    def get_ordering_queryset(self):
        return self.product.images.all()


class Deals(models.Model):
    name = models.CharField(max_length=20)
    content = models.TextField(blank=True, verbose_name=u"Описание акции")
    created_at = models.DateTimeField(auto_now=True, db_index=True)

    image = VersatileImageField(
        'Изображение',
        upload_to='images/',
        width_field='width',
        height_field='height',
        ppoi_field='ppoi',
        blank=True
    )
    height = models.PositiveIntegerField(
        'Высота',
        blank=True,
        null=True
    )
    width = models.PositiveIntegerField(
        'Ширина',
        blank=True,
        null=True
    )
    ppoi = PPOIField(
        'Image PPOI'
    )

    class Meta:
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'

    def get_image(self):
        return self.image if self.image else None

    def __str__(self):
        return self.name
